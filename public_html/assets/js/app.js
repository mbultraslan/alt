var app = {

    apiEndPoint: 'https://bayrak.co.uk/alt/api/',
    exportEndPoint: 'https://bayrak.co.uk/alt/exports/',

    countries: {},


    init: function () {
        app.getCountries();
        app.getAddresses();
    },


    getAddresses: function () {
        let target = '#address_table';
        let url = app.apiEndPoint + '?action=get_addresses';


        let old_table = $(target).DataTable();
        old_table.destroy();

        $(target).DataTable({
            "lengthChange": true,
            "deferRender": true,
            "processing": true,
            "serverSide": true,
            "ordering": false,
            "ajax": {
                url: url,
                type: "post",
                error: function () {  // error handling
                    alert('No record founded or something went wrong!')
                }
            }
        });
    },

    addRecordForm: function () {
        app.renderForm();
        $('#record_modal').modal();
    },

    submitForm() {
        $.ajax({
            url: app.apiEndPoint + '?action=add_record',
            type: 'post',
            contentType: 'application/x-www-form-urlencoded',
            data: $('#data_form').serialize(),
            success: function (data, textStatus, jQxhr) {
                if (data.result === true) {
                    $('#record_modal').modal('hide');
                    app.getAddresses();
                    bootbox.alert(data.message + ' Record ID:'+ data.id);
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    },

    export() {
        $.ajax({
            url: app.apiEndPoint + '?action=export',
            type: 'post',
            data: $('#data_form').serialize(),
            success: function (data, textStatus, jQxhr) {
                if (data.result === true) {
                    bootbox.alert('<p>Your Export is generated. <a href="'+app.exportEndPoint+data.filename+'" target="_blank">Download</a></p>');
                }else{
                    bootbox.alert('An Error Occurred');
                }
            },
            error: function (jqXhr, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    },


    deleteRecord : function(id){
        bootbox.confirm({
            message: "Are You sure",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function(result){
                if(result){
                    $.ajax({
                        url: app.apiEndPoint + '?action=delete_record',
                        type: 'post',
                        contentType: 'application/x-www-form-urlencoded',
                        data: {id:id},
                        success: function (data, textStatus, jQxhr) {
                            if (data.result === true) {
                                app.getAddresses();
                                bootbox.alert(data.message);
                            }
                        },
                    });
                }
            }
        });
    },

    getCountries: function () {
        $.get(app.apiEndPoint + '?action=get_countries', function (data) {
            app.countries = data;
        })
    },


    renderForm: function (data) {
        var form = '<form class="form-horizontal" id="data_form">';
        form += '                    <div class="form-group">';
        form += '                        <label for="first_name" class="col-sm-4 control-label">First Name</label>';
        form += '                        <div class="col-sm-8">';
        form += '                            <input type="text" class="form-control" id="first_name" name="first_name" required>';
        form += '                        </div>';
        form += '                    </div>';
        form += '                    <div class="form-group">';
        form += '                        <label for="last_name" class="col-sm-4 control-label">Last Name</label>';
        form += '                        <div class="col-sm-8">';
        form += '                            <input type="text" class="form-control" id="last_name" name="last_name" required>';
        form += '                        </div>';
        form += '                    </div>';
        form += '                    <div class="form-group">';
        form += '                        <label for="phone" class="col-sm-4 control-label">Phone</label>';
        form += '                        <div class="col-sm-8">';
        form += '                            <input type="text" class="form-control" id="phone" name="phone" required>';
        form += '                        </div>';
        form += '                    </div>';
        form += '                    <div class="form-group">';
        form += '                        <label for="email" class="col-sm-4 control-label">Email</label>';
        form += '                        <div class="col-sm-8">';
        form += '                            <input type="email" class="form-control" id="email" name="email" required>';
        form += '                        </div>';
        form += '                    </div>';
        form += '                    <div class="form-group">';
        form += '                        <label for="address_1" class="col-sm-4 control-label">Address 1</label>';
        form += '                        <div class="col-sm-8">';
        form += '                            <input type="text" class="form-control" id="address_1" name="address_1" required>';
        form += '                        </div>';
        form += '                    </div>';
        form += '                    <div class="form-group">';
        form += '                        <label for="address_2" class="col-sm-4 control-label">Address 2</label>';
        form += '                        <div class="col-sm-8">';
        form += '                            <input type="text" class="form-control" id="address_2" name="address_2" required>';
        form += '                        </div>';
        form += '                    </div>';
        form += '                    <div class="form-group">';
        form += '                        <label for="city" class="col-sm-4 control-label">City</label>';
        form += '                        <div class="col-sm-8">';
        form += '                            <input type="text" class="form-control" id="city" name="city" required>';
        form += '                        </div>';
        form += '                    </div>';
        form += '                    <div class="form-group">';
        form += '                        <label for="country" class="col-sm-4 control-label">Country</label>';
        form += '                        <div class="col-sm-8">';
        form += '                            <select class="form-control" id="country" name="country" required>';
        $.each(app.countries, function (key, value) {
            form += '                                    <option value="' + value.id + '">' + value.name + '</option>'
        });
        form += '                             </select>';
        form += '                        </div>';
        form += '                    </div>';
        form += '                    <div class="form-group">';
        form += '                        <label for="post_code" class="col-sm-4 control-label">Post Code</label>';
        form += '                        <div class="col-sm-8">';
        form += '                            <input type="text" class="form-control" id="post_code" name="post_code" required>';
        form += '                        </div>';
        form += '                    </div>';
        form += '                    <div class="form-group">';
        form += '                        <label for="notes" class="col-sm-4 control-label">Notes</label>';
        form += '                        <div class="col-sm-8">';
        form += '                            <textarea name="notes" id="notes" class="form-control" cols="30" rows="10" required></textarea>';
        form += '                        </div>';
        form += '                    </div>';
        form += '                </form>';
        $('#form_body').html(form);
    }
}