<?php


namespace Altitude\Libs;


class Model {
	// Hold the class instance.
	private static $instance = NULL;

	public static $db = NULL;

	// The constructor is private
	// to prevent initiation with outer code.
	private function __construct()
	{
		if (self::$db == NULL)
		{
			self::$db = new \SQLite3(APP_DIR . DS . 'database.db');
		}
	}

	// The object is created from within the class itself
	// only if the class has no instance.
	public static function getInstance()
	{
		if (self::$instance == NULL)
		{
			self::$instance = new Model();
		}

		return self::$instance;
	}
}