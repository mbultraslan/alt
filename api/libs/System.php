<?php


namespace Altitude\Libs;


class System
{

    public static function response($data, $format = DEFAULT_RESPONSE_FORMAT, $httpStatus = 200)
    {
        self::setHttpStatus($httpStatus);
        self::setContentHeader(DEFAULT_RESPONSE_FORMAT);
        self::disableCache();
        if($format === 'json'){
            echo json_encode($data);
        }else{
            echo $data;
        }
        exit;
    }


    public static function setHttpStatus($code)
    {
        $http = array(
            200 => 'HTTP/1.1 200 OK',
            400 => 'HTTP/1.1 400 Bad Request',
            403 => 'HTTP/1.1 403 Forbidden',
            404 => 'HTTP/1.1 404 Not Found',
            500 => 'HTTP/1.1 500 Internal Server Error',
            504 => 'HTTP/1.1 504 Gateway Time-out',
            505 => 'HTTP/1.1 505 HTTP Version Not Supported',
        );

        //  return 500 if HTTP code invalid
        $code = isset($http[$code]) ? $code : 500;

        header($http[$code]);

        return
            array(
                'code' => $code,
                'error' => $http[$code],
            );
    }

    public static function setContentHeader($type){

        switch ($type){
            case 'json':
                header('Content-Type: application/json');
                break;
            case 'xml':
                header('Content-Type: application/xml; charset=utf-8');
                break;
            default:
                header('Content-Type: application/json');
        }

    }


    public static function disableCache(){
        // Disable caching of the current document:
        header('Cache-Control: no-cache, no-store, max-age=0, must-revalidate');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Pragma: no-cache');
    }

}