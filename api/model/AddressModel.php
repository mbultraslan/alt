<?php


namespace Altitude\Model;

use Altitude\Libs\Model;

class AddressModel extends Model
{

    public static function getAllRecords()
    {
        self::getInstance();
        $sql = "SELECT a.*, c.name AS country FROM addresses a LEFT JOIN  countries c ON  a.id_country = c.id  ORDER BY a.id DESC";
        $results = self::$db->query($sql);
        $records = [];
        while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
            $records[] = $row;
        }
        return $records;
    }


    public static function getRecords($start = false, $length = false, $search = false)
    {
        self::getInstance();
        $limit = $length ? ' LIMIT ' . $start . ',' . $length : '';
        $like = $search ? " AND (first_name LIKE '%" . $search . "%' OR last_name  LIKE '%" . $search . "%' OR phone  LIKE '%" . $search . "%' OR email  LIKE '%" . $search . "%')" : "";
        $sql = "SELECT a.*, c.name AS country FROM addresses a LEFT JOIN  countries c ON  a.id_country = c.id  WHERE 1=1 " . $like . " ORDER BY a.id DESC" . $limit;
        $results = self::$db->query($sql);
        $records = [];
        while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
            $records[] = $row;
        }
        return $records;
    }

    public static function getTotalRecords($search = false)
    {
        self::getInstance();
        $like = $search ? " AND (first_name LIKE '%" . $search . "%' OR last_name  LIKE '%" . $search . "%' OR phone  LIKE '%" . $search . "%' OR email  LIKE '%" . $search . "%')" : "";
        $sql = "SELECT COUNT(*) AS cnt FROM addresses  WHERE 1=1 " . $like;
        $results = self::$db->query($sql);
        $row = $results->fetchArray(SQLITE3_ASSOC);
        return isset($row['cnt']) ? $row['cnt'] : 0;
    }


    public static function getRecordById($id)
    {
        self::getInstance();
        $sql = "SELECT a.*, c.name AS country FROM addresses a LEFT JOIN  countries c ON  a.id_country = c.id  WHERE a.id=" . $id . " LIMIT 1";
        $results = self::$db->query($sql);
        $row = $results->fetchArray(SQLITE3_ASSOC);
        return isset($row['id']) ? $row : FALSE;

    }


    public static function deleteRecordById($id)
    {
        self::getInstance();
        $sql = "DELETE FROM addresses  WHERE id=" . $id;
        self::$db->query($sql);

    }


    public static function addRecord($data)
    {
        self::getInstance();
        $sql = "INSERT INTO addresses(first_name, last_name, phone, email, address_1, address_2, city, id_country, postcode, notes) 
				VALUES(
						'" . self::$db->escapeString($data['first_name']) . "',
						'" . self::$db->escapeString($data['last_name']) . "',
						'" . self::$db->escapeString($data['phone']) . "',
						'" . self::$db->escapeString($data['email']) . "',
						'" . self::$db->escapeString($data['address_1']) . "',
						'" . self::$db->escapeString($data['address_2']) . "',
						'" . self::$db->escapeString($data['city']) . "',
						'" . (int)$data['country'] . "',
						'" . self::$db->escapeString($data['post_code']) . "',
						'" . self::$db->escapeString($data['notes']) . "' 
				)";
        self::$db->query($sql);
        return self::$db->lastInsertRowID();
    }


    public static function updateRecord($id, $data)
    {
        self::getInstance();
        echo $sql = "UPDATE addresses 
				SET
					first_name ='" . self::$db->escapeString($data['first_name']) . "',
					last_name ='" . self::$db->escapeString($data['last_name']) . "',
					phone ='" . self::$db->escapeString($data['phone']) . "',
					email ='" . self::$db->escapeString($data['email']) . "',
					address_1 ='" . self::$db->escapeString($data['address_1']) . "',
					address_2 ='" . self::$db->escapeString($data['address_2']) . "',
					city ='" . self::$db->escapeString($data['city']) . "',
					id_country ='" . (int)$data['country'] . "',
					postcode ='" . self::$db->escapeString($data['post_code']) . "',
					notes ='" . self::$db->escapeString($data['notes']) . "'
				WHERE
					id = " . (int)$id;
        self::$db->query($sql);
    }


    public static function getCountries()
    {
        self::getInstance();
        $sql = "SELECT * FROM countries ORDER BY name ASC";
        $results = self::$db->query($sql);
        $countries = [];
        while ($row = $results->fetchArray(SQLITE3_ASSOC)) {
            $countries[] = $row;
        }
        return $countries;
    }


}