<?php


namespace Altitude\Controller;


use Altitude\Model\HomeModel;

/**
 * Class HomeController
 *
 * @author  Mehmet
 * @since   0.0.1
 * @package Altitude\Controller
 */
class HomeController {


	public function dataBaseExist()
	{
		return HomeModel::dataBaseExist();
	}


	public function createDataBase()
	{

		HomeModel::createDataBase();


	}

}