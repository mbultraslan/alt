<?php


namespace Altitude\Controller;


use Altitude\Libs\System as System;
use Altitude\Model\AddressModel;

/**
 * Class AddressController
 *
 * @author Mehmet
 * @package Altitude\Controller
 * @since 0.0.1
 */
class AddressController
{

    public function getAddresses($post){

        $start = $post['start'];
        $length = $post['length'];
        $search = $post['search']['value'];

        $total = AddressModel::getTotalRecords($search);
        $records = AddressModel::getRecords($start, $length, $search);

        $data = [];

        foreach ($records as $record){
            $data[] = [
                $record['id'],
                $record['first_name'],
                $record['last_name'],
                $record['phone'],
                $record['email'],
                $record['address_1'],
                $record['address_2'],
                $record['city'],
                $record['country'],
                $record['postcode'],
                $record['notes'],
                '<a class="btn btn-warning" onclick="app.editRecord('.$record['id'].')">Edit</a> <a class="btn btn-danger" onclick="app.deleteRecord('.$record['id'].')">Delete</a>'
            ];


        }

        $result = array(
            "draw" => (int)$_REQUEST['draw'],
            "recordsTotal" => (int)$total,
            "recordsFiltered" => (int)$total,
            "data" => $data
        );
        System::response($result);


    }


    public function exportRecord(){
        $records = AddressModel::getAllRecords();

        if (!file_exists(EXPORT_DIR)) {
            mkdir(EXPORT_DIR, 0777, true);
        }


        $file= 'export_'.time().'.json';

        $fp = fopen(EXPORT_DIR.DS.$file, 'w');
        fwrite($fp, json_encode($records));
        fclose($fp);

        System::response(['result'=>TRUE,  'filename'=>$file]);

    }

	public function addRecord($data){
		$id = AddressModel::addRecord($data);
		if($id){
			System::response(['result'=>TRUE,  'message'=>'Record added', 'id'=>$id]);
		}else{
			System::response(['result'=>FALSE,  'message'=>'Record could not added', 'id'=>0]);
		}
	}



	public function updateRecord($id, $data){
		$id = AddressModel::updateRecord($id, $data);
		if($id){
			System::response(['result'=>TRUE, 'message'=>'Record Updated', 'id'=>$id]);
		}else{
			System::response(['result'=>FALSE,  'message'=>'Record could not updated', 'id'=>0]);
		}
	}



    public function deleteRecord($post){
        $id = (int)$post['id'];
        $record = AddressModel::getRecordById($id);
        if($record){
            AddressModel::deleteRecordById($id);
            System::response(['result'=>TRUE, 'message'=>$record['first_name']. ' Deleted']);
        }else{
            System::response(['result'=>FALSE, 'message'=>'Record not exist']);
        }
    }

	public function getCountries(){
    	$countries = AddressModel::getCountries();
		System::response($countries);
	}

}