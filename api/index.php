<?php
// allow origin for ajax CORS
header("Access-Control-Allow-Origin: *");
require_once __DIR__ . '/vendor/autoload.php';


$home = new \Altitude\Controller\HomeController();
if (false === $home->dataBaseExist()) {
    $home->createDataBase();
}


if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'get_countries' :
          $cont = new \Altitude\Controller\AddressController();
          $cont->getCountries();
            break;
            case 'get_addresses' :
          $cont = new \Altitude\Controller\AddressController();
          $cont->getAddresses($_POST);
            break;
        case 'add_record':
            $cont = new \Altitude\Controller\AddressController();
            $cont->addRecord($_POST);
            break;
        case 'delete_record':
            $cont = new \Altitude\Controller\AddressController();
            $cont->deleteRecord($_POST);
            break;
        case 'export':
            $cont = new \Altitude\Controller\AddressController();
            $cont->exportRecord();
            break;
        default:
            //Exception here
            die('invalid action');
    }

} else {
    //Exception here
    die('invalid action');
};